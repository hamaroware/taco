# README #

Progetto Taco è stato realizzato per la traccia d'esame di principi e progettazione di software dell'anno 2015/2016 dell'Università del Salento.

### Come posso visualizzare il progetto? ###

* Clona la repo
* Importa nel tuo db il file tacodb.sql che trovi dentro dbconnetions/mysql
* Importa nel tuo IDE le librerie mancanti che devi recuperare in rete
* I dati di accesso di alcuni utenti sono riportati più in basso

### Cosa posso fare? ###

Diciamo nulla, considerando che il progetto è stato portato a termine e non ci saranno altri commit.

### Ho bisogno di aiuto, a chi chiedo? ###

* GoodNello (Giulio - giulio.albanese (at) outlook.com)
* saucermann (Tommaso - tommaso.paladini (at) outlook.com)